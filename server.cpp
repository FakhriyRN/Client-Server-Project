/*
	Nama 	: Fakhriy Ramadhan N.
	NRP		: 4210171027
	Kelas	: 1-D4 Teknologi Game
	
	cpp for server 

*/

#include "stdafx.h"
#include <WS2tcpip.h>
#include <iostream>

using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main(){
	// variable result untuk menyimpan nilai kembali saat memanngil fungsi
	int result;
	
	// WS= winsock, tempat mengisi data ketika memanggil fungsi WSAStartup()
	WSAData wsaData;
	
	//port yang digunakan
	int DEFAULT_PORT = 5858;
	
	// inisialisasi winsock
	cout<<"Initializing winsock....."<< endl;
	result = WSAStartup(MAKEWORD(2,2),&wsaData);
	
	// apabila inisialisasi gagal
	if (result != 0){
		cout<<"Error initializing : "<<WSAGetLastError()<<endl;
		return 1;
	}
	
	// tanda untuk menyimpan informasi jaringan
	sockaddr_in hints;
	
	// set IPv4
	hints.sin_family = AF_INET;
	
	// set Port
	hints.sin_port = htons(DEFAULT_PORT);
	
	// set alamat untuk server
	hints.sin_addr.S_un.S_addr = INADDR_ANY;
	
	// membuat socket untuk server
	cout<< "Creating Socket....."<<endl;
	SOCKET sock = socket(AF_INET,SOCK_STREAM,0);
	
	// jika gagal membuat socket
	if (sock == INVALID_SOCKET){
		cout<<"Failed to create....."<<endl;
		WSACleanup();
		return 1;
	}
	
	// binding socket yang telah dibuat
	result = bind(sock,(SOCKADDR*)&hints,sizeof(hints));
	
	if (result != 0){
		cout<<"Failed to bind socket....."<<endl;
		WSACleanup();
		return 1;
	}
	
	cout<<"Server Ready....."<<endl;
	cout<<"Waiting for Client....."<<endl;
	
	// membuka akses untuk client
	result = listen(sock,SOMAXCONN);
	
	if (result != 0){
		cout<<"Failed to listening socket....."<<endl;
		WSACleanup();
		return 1;
	}
	
	// client berhasil menyambung, menerima socket client dan masuk ke var socket client
	SOCKET client = accept(sock,NULL,NULL);
	
	// jika socket client tidak cocok
	if (client == INVALID_SOCKET){
		cout<<"Failed to accept client socket....."<<endl;
		WSACleanup();
		return 1;
	}
	
	cout<<"Client Connected....."<<endl;
	
	char buff[512];
	
	// proses menerima dan menampilkan data yang dikirim oleh client
	do{
		result = recv(client,buff,512,0);
		
		if (result > 0){
			cout<<"Data Received : "<<buff<<endl;
		}
		
		else if (result == 0){
			cout<<"Client Disconnected....."<<endl;
		}
	}
	
	while (result>0);
	
	//menutup socket server
	closesocket(sock);
	
	//menutup socket client
	closesocket(client);
	
	WSACleanup();
	
	return 0;
		
}
