#include "stdafx.h"
#include <winsock2.h>
#include <iostream>
#include <stdio.h>

using namespace std;

#pragma comment(lib, "Ws2_32.lib")

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

int main()
{
	SOCKET socketS;
	int DEFAULT_PORT = 5858;

	InitWinsock();
	struct sockaddr_in local;

	local.sin_family = AF_INET;
	local.sin_port = htons(DEFAULT_PORT);
	local.sin_addr.s_addr = INADDR_ANY;

	struct sockaddr_in from;
	int fromlen = sizeof(from);
	
	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	bind(socketS, (SOCKADDR *)&local, sizeof(local));
	cout << "Server Up..." << endl;

	char buffer[512];

	cout << "\ngoing to receive message...\n";
	do {
		int rc = recvfrom(socketS, buffer, sizeof(buffer), 0, (struct sockaddr *)&from, &fromlen);
		if (rc>0)
		{
			cout << "ERROR READING FROM SOCKET";
		}
		cout << "\n the message received is : " << buffer << endl;

		int rp = sendto(socketS, "hi", 2, 0, (struct sockaddr *)&from, fromlen);

		if (rp<0)
		{
			cout << "ERROR writing to SOCKET";
		}
	} while (1);

	closesocket(socketS);

	return 0;
}
